import { combineReducers } from 'redux'
import suggest from './suggest'

const sixtApp = combineReducers({
  suggest
})

export default sixtApp
