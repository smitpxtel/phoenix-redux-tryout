import React, { PropTypes, Component } from 'react'
import { connect } from 'react-redux'
import InstanceBox from './InstanceBox'
import { hide_list } from '../../actions/hide_list'

class SuggestFormBox extends Component {

    constructor (props) {
        super(props)
        this.SuggestFormBox =  false

        document.addEventListener('click', (e)=>{this.handleDocumentClick(e)}, false)
    }

    handleDocumentClick(e) {
        if(!this.SuggestFormBox.contains(e.target)) {
            this.props.dispatch( hide_list() )
        }
        return true;
    }

    render() {
        return (
            <div ref={node=>{this.SuggestFormBox = node}} id="suggestFormBox" className="suggestFormBox">
                <form action="#">
                    <div className="suggestPickup">
                        <InstanceBox instance='pickupData' />
                    </div>
                    <div className="suggestReturn">
                        <InstanceBox instance='returnData' />
                    </div>
                </form>
            </div>
        )
    }
}

SuggestFormBox = connect()(SuggestFormBox)

export default SuggestFormBox