import React, { PropTypes, Component } from 'react'
import { connect } from 'react-redux'
import { select_station } from '../../actions/select_station'

class SuggestList extends Component {

    constructor (props) {
        super(props)
        this.response       =  false
        this.header         =  false
        this.wrapperClass   =  "suggestListWrapper suggestListWrapper" + this.props.instance
        this.contentClass   =  "suggestListContent suggestListContent" + this.props.instance
        this.listContentRef =  false;
    }

    handleSuggestClick(e, station, selIndex) {
        this.props.dispatch( select_station(this.props.instance, station, selIndex) )
    }

    componentDidUpdate(nextProps) {
/*
        console.info('----componentDidUpdate-----')
        console.info(this.refs['foo'+this.selIndex])
        console.info(this.selIndex)
        console.info('----componentDidUpdate-----')


*/


        if(typeof this.refs['suggestStation'+this.selIndex] === 'undefined') {
            return false;
        }


        let itemOffset =  this.refs['suggestStation'+this.selIndex].offsetTop;
        this.listContentRef.scrollTop =  itemOffset

    }

    componentWillUpdate(nextProps) {
        this.response   =  nextProps.response? nextProps.response : false;
        this.selIndex   =  nextProps.selIndex;
        this.listDpl    =  nextProps.listDpl;
    }

    getHeader(station) {
        let headerHtml =  false
        if(station.category !== this.header) {
            headerHtml =  <h3>{station.category}</h3>
        }
        this.header =  station.category
        return headerHtml
    }

    createSuggestList() {
        return (
            this.response.map((station, index) => {
                let className =  (index === this.selIndex)? 'suggestSelected' : 'suggestNorm'
                return (
                    <div key={index} ref={'suggestStation'+index}>
                        {this.getHeader(station)}
                        <p
                            className={className}
                            onClick={(e) => this.handleSuggestClick(e, station, index)}
                        >{index} {station.name}</p>
                    </div>
                )
            })
        )
    }

    getSuggestList() {
        return this.response? this.createSuggestList() : "";
    }

    render() {
        let contentClass =  this.contentClass + (this.listDpl? ' suggestListContentOpen' : '')
        return (
            <div className={this.wrapperClass}>
                <div className={contentClass} ref={node => {this.listContentRef = node}}>
                    {this.getSuggestList()}
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state, ownProps) => {
    let suggestData =  state.suggest[ownProps.instance];
    return { response:suggestData.response,listDpl:suggestData.listDpl,selIndex:suggestData.selIndex }
}

SuggestList = connect(mapStateToProps)(SuggestList)

export default SuggestList