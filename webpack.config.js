module.exports = {
    entry: "./index.js",
    output: {
        path: __dirname + "/static/",
        filename: "bundle.js"/*,
        chunkFilename: "[id].bundle.js",
        publicPath: "http://localhost:3000/"*/
    },
    module: {
        loaders: [
            {
                //tell webpack to use jsx-loader for all *.jsx files
                test: /\.jsx?$/,
                exclude: /node_modules/,
                loader: 'babel',
                include: __dirname,
                query: {
                    presets: ['es2015', 'react']
                }
            }
        ]
    }
}
