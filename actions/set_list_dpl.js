export const set_list_dpl = (instance, listDpl) => {
    return {
        type: 'SET_LIST_DPL',
        instance: instance,
        listDpl: listDpl
    }
}
