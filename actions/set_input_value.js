export const set_input_value = (instance, inpValue) => {
    return {
        type: 'SET_INPUT_VALUE',
        instance: instance,
        inpValue: inpValue
    }
}
