# Station Suggest consuming the Sixt OBE-light API with Google Maps #

React-Redux tryout with react-thunk as middleware for asynchronus request. The setup must be done manually to give you a quick overview about the used modules. Feel free to create your own package.json or integrat hot-reloading. run "webpack" while editing to update build.js

You should have the npm http-server or any other npm server installed. http-server on port 3000 you can start simply by:
http-server -p 3000

## Node modules ##
npm install --save react react-dom babel-core babel-preset-react babel-loader babel-preset-es2015 react-redux redux redux-thunk webpack

## Use ##
webpack



## Learnings ##
To set component properties like 'this.item' always use

```
#!javascript
componentWillUpdate(nextProps) {
    this.item =  nextProps.item
}
```


Using 'this.props.item' here may result in async behavior!